import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { NzTableModule } from 'ng-zorro-antd/table';
import { PeopleListComponent } from './components/people-list/people-list.component';
import { HeaderComponent } from './components/header/header.component';
import {
  NzButtonModule,
  NzLayoutModule,
  NzModalModule,
  NzSpinModule,
} from 'ng-zorro-antd';
import { UniqueCountComponent } from './components/unique-count/unique-count.component';
import { DuplicatedComponent } from './components/duplicated/duplicated.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    PeopleListComponent,
    HeaderComponent,
    UniqueCountComponent,
    DuplicatedComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzTableModule,
    NzModalModule,
    NzSpinModule,
    NzButtonModule,
    NzLayoutModule,
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent],
})
export class AppModule {}
