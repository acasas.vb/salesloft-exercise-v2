import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ApiService } from './api.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

import { environment } from '../../environments/environment';

describe('ApiService', () => {
  let service: ApiService;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return the list of people', () => {
    // spy and mock httpClient
    const mockedPeople = require('../../assets/people.json');
    httpClient = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
    spyOn(httpClient, 'get').and.returnValue(of(mockedPeople));

    service = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;
    const spy = jasmine.createSpy('spy');
    service.getPeople$().subscribe(spy);

    // verify that the service returned mocked data
    expect(spy).toHaveBeenCalledWith(mockedPeople);

    // verify that the service called the proper http endpoint
    expect(httpClient.get).toHaveBeenCalledWith(
      `${environment.api}/people?page=1&pageSize=25`
    );
  });

  it('should return the list of people with given page and pageSize', () => {
    // spy and mock httpClient
    const mockedPeople = require('../../assets/people.json');
    httpClient = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
    spyOn(httpClient, 'get').and.returnValue(of(mockedPeople));

    service = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;
    const spy = jasmine.createSpy('spy');
    service.getPeople$({ page: 2, pageSize: 10 }).subscribe(spy);

    // verify that the service returned mocked data
    expect(spy).toHaveBeenCalledWith(mockedPeople);

    // verify that the service called the proper http endpoint
    expect(httpClient.get).toHaveBeenCalledWith(
      `${environment.api}/people?page=2&pageSize=10`
    );
  });

  it('should return unique counts', () => {
    // spy and mock httpClient
    const counts = require('../../assets/counts.json');
    httpClient = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
    spyOn(httpClient, 'get').and.returnValue(of(counts));

    service = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;
    const spy = jasmine.createSpy('spy');
    service.getUniqueCounts$().subscribe(spy);

    // verify that the service returned mocked data
    expect(spy).toHaveBeenCalledWith(counts);

    // verify that the service called the proper http endpoint
    expect(httpClient.get).toHaveBeenCalledWith(
      `${environment.api}/people/unique-count?pageSize=100`
    );
  });

  it('should return duplicated email addresses', () => {
    // spy and mock httpClient
    const duplicated = require('../../assets/duplicated.json');
    httpClient = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
    spyOn(httpClient, 'get').and.returnValue(of(duplicated));

    service = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;
    const spy = jasmine.createSpy('spy');
    service.getDuplicated$().subscribe(spy);

    // verify that the service returned mocked data
    expect(spy).toHaveBeenCalledWith(duplicated);

    // verify that the service called the proper http endpoint
    expect(httpClient.get).toHaveBeenCalledWith(
      `${environment.api}/people/duplicated?precision=0.86`
    );
  });
});
