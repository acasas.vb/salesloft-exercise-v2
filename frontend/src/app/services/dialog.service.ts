import { Injectable } from '@angular/core';
import { ModalOptions, NzModalService } from 'ng-zorro-antd';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(private dialog: NzModalService) {}

  open(component, config: ModalOptions = {}) {
    this.dialog.create({
      nzFooter: null,
      ...config,
      nzContent: component,
    });
  }
}
