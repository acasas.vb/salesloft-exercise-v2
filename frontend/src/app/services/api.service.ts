import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';

type getPeopleParams = {
  page?: number;
  pageSize?: number;
};

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getPeople$(params: getPeopleParams = { page: 1, pageSize: 25 }) {
    const { page, pageSize } = params;
    return this.http.get<any>(
      `${environment.api}/people?page=${page}&pageSize=${pageSize}`
    );
  }

  getUniqueCounts$(pageSize: number = 100) {
    return this.http.get<any>(
      `${environment.api}/people/unique-count?pageSize=${pageSize}`
    );
  }

  getDuplicated$(precision: number = 0.86) {
    return this.http.get<any>(
      `${environment.api}/people/duplicated?precision=${precision}`
    );
  }
}
