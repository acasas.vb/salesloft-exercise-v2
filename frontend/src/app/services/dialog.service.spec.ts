import { TestBed } from '@angular/core/testing';
import { spyOnClass } from 'jasmine-es6-spies';
import { NzModalModule, NzModalService } from 'ng-zorro-antd';
import { UniqueCountComponent } from '../components/unique-count/unique-count.component';

import { DialogService } from './dialog.service';

describe('DialogService', () => {
  let service: DialogService;
  let dialog: jasmine.SpyObj<NzModalService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NzModalModule],
      providers: [
        {
          provide: NzModalService,
          useFactory: () => spyOnClass(NzModalService),
        },
      ],
    });
    service = TestBed.inject(DialogService);
    dialog = TestBed.inject(NzModalService) as jasmine.SpyObj<NzModalService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call create from ng-zorro', () => {
    service.open(UniqueCountComponent);
    expect(dialog.create).toHaveBeenCalled();
  });
});
