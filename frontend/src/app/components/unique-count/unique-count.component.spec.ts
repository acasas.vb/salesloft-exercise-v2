import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { spyOnClass } from 'jasmine-es6-spies';
import { of } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';

import { UniqueCountComponent } from './unique-count.component';

describe('UniqueCountComponent', () => {
  let component: UniqueCountComponent;
  let fixture: ComponentFixture<UniqueCountComponent>;
  let apiService: jasmine.SpyObj<ApiService>;

  const dom = (selector: string) => {
    return fixture.nativeElement.querySelector(selector);
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UniqueCountComponent],
      providers: [
        { provide: ApiService, useFactory: () => spyOnClass(ApiService) },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniqueCountComponent);
    component = fixture.componentInstance;

    const count = require('../../../assets/counts.json');
    apiService = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;
    apiService.getUniqueCounts$.and.returnValue(of(count));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have the table', () => {
    expect(dom('[data-test="unique-counts-table"]')).toBeTruthy();
  });

  it('should display table headers', () => {
    expect(dom('[data-test="letter-header"]').textContent).toContain('Letter');
    expect(dom('[data-test="count-header"]').textContent).toContain('Count');
  });

  it(`should unsubscribe on destroy`, () => {
    component.ngOnDestroy();
    expect(component.subscription.closed).toBeTruthy();
  });

  it(`should NOT unsubscribe on destroy if subscription is null`, () => {
    component.subscription = null;
    component.ngOnDestroy();
    expect(component.subscription).toBeNull();
  });
});
