import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-unique-count',
  templateUrl: './unique-count.component.html',
  styleUrls: ['./unique-count.component.scss'],
})
export class UniqueCountComponent implements OnInit, OnDestroy {
  counts;
  subscription: Subscription;
  loading: boolean = true;

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.subscription = this.apiService
      .getUniqueCounts$()
      .subscribe((counts) => {
        this.counts = counts;
        this.loading = false;
      });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
