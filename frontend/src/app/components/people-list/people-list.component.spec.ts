import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ApiService } from 'src/app/services/api.service';
import { spyOnClass } from 'jasmine-es6-spies';
import { of } from 'rxjs';

import { PeopleListComponent } from './people-list.component';
import { NzTableModule } from 'ng-zorro-antd/table';

describe('PeopleListComponent', () => {
  let component: PeopleListComponent;
  let fixture: ComponentFixture<PeopleListComponent>;
  let apiService: jasmine.SpyObj<ApiService>;

  const dom = (selector: string) => {
    return fixture.nativeElement.querySelector(selector);
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NzTableModule],
      declarations: [PeopleListComponent],
      providers: [
        { provide: ApiService, useFactory: () => spyOnClass(ApiService) },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeopleListComponent);
    component = fixture.componentInstance;

    const people = require('../../../assets/people.json');
    apiService = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;
    apiService.getPeople$.and.returnValue(of(people));
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have the table', () => {
    expect(dom('[data-test="people-table"]')).toBeTruthy();
  });

  it('should display table headers', () => {
    expect(dom('[data-test="name-header"]').textContent).toContain('Name');
    expect(dom('[data-test="email-header"]').textContent).toContain(
      'Email Address'
    );
    expect(dom('[data-test="title-header"]').textContent).toContain(
      'Job Title'
    );
  });

  it(`should display person's info`, () => {
    let row = fixture.nativeElement.querySelector('tbody tr');
    expect(row.querySelector('td:nth-child(1)').innerText).toEqual(
      'Pat Johnson'
    );
    expect(row.querySelector('td:nth-child(2)').innerText).toEqual(
      'pat.johnson@example.com'
    );
    expect(row.querySelector('td:nth-child(3)').innerText).toEqual(
      'Sales Development Representative'
    );
  });

  it(`should unsubscribe on destroy`, () => {
    component.ngOnDestroy();
    expect(component.subscription.closed).toBeTruthy();
  });

  it(`should NOT unsubscribe on destroy if subscription is null`, () => {
    component.subscription = null;
    component.ngOnDestroy();
    expect(component.subscription).toBeNull();
  });

  it('should change page', () => {
    component.changePage(2);
    expect(component.currentPage).toEqual(2);
  });

  it('should display 36 as page number', () => {
    expect(dom('.ant-pagination-item:nth-last-child(2) a').innerText).toEqual(
      '36'
    );
  });

  it('should display the right number of columns', () => {
    expect(fixture.nativeElement.querySelectorAll('tr').length).toEqual(26);
  });
});
