import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss'],
})
export class PeopleListComponent implements OnInit, OnDestroy {
  people = [];
  total = 0;
  pages = 0;
  currentPage = 1;
  currentPageSize = 10;
  subscription: Subscription;

  constructor(private API: ApiService) {}

  getPeople(): void {
    this.subscription = this.API.getPeople$({
      page: this.currentPage,
      pageSize: this.currentPageSize,
    }).subscribe((response) => {
      this.people = response.data;
      this.total = response.metadata.paging['total_count'];
      this.pages = response.metadata.paging['total_pages'];
    });
  }

  ngOnInit(): void {
    this.getPeople();
  }

  changePage(pageNumber: number): void {
    this.currentPage = pageNumber;
    this.getPeople();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
