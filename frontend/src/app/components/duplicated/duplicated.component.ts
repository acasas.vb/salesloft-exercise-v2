import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-duplicated',
  templateUrl: './duplicated.component.html',
  styleUrls: ['./duplicated.component.scss'],
})
export class DuplicatedComponent implements OnInit {
  duplicated = [];
  subscription: Subscription;

  loading: boolean = true;

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.subscription = this.apiService
      .getDuplicated$()
      .subscribe((duplicated) => {
        this.duplicated = duplicated;
        this.loading = false;
      });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
