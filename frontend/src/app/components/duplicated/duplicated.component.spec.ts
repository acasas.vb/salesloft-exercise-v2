import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { spyOnClass } from 'jasmine-es6-spies';
import { of } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';

import { DuplicatedComponent } from './duplicated.component';

describe('DuplicatedComponent', () => {
  let component: DuplicatedComponent;
  let fixture: ComponentFixture<DuplicatedComponent>;
  let apiService: jasmine.SpyObj<ApiService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DuplicatedComponent],
      providers: [
        { provide: ApiService, useFactory: () => spyOnClass(ApiService) },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuplicatedComponent);
    component = fixture.componentInstance;

    const duplicated = require('../../../assets/duplicated.json');
    apiService = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;
    apiService.getDuplicated$.and.returnValue(of(duplicated));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should unsubscribe on destroy`, () => {
    component.ngOnDestroy();
    expect(component.subscription.closed).toBeTruthy();
  });

  it(`should NOT unsubscribe on destroy if subscription is null`, () => {
    component.subscription = null;
    component.ngOnDestroy();
    expect(component.subscription).toBeNull();
  });
});
