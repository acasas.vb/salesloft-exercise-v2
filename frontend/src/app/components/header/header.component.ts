import { Component, OnInit } from '@angular/core';
import { DialogService } from 'src/app/services/dialog.service';
import { DuplicatedComponent } from '../duplicated/duplicated.component';
import { UniqueCountComponent } from '../unique-count/unique-count.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(private dialogService: DialogService) {}
  counts$;

  ngOnInit(): void {}

  displayUniqueCounts() {
    this.dialogService.open(UniqueCountComponent, { nzTitle: 'Unique Counts' });
  }

  displayDuplicated() {
    this.dialogService.open(DuplicatedComponent, {
      nzTitle: 'Duplicated Email Addresses',
    });
  }
}
