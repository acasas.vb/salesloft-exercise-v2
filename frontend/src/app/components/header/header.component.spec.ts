import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { spyOnClass } from 'jasmine-es6-spies';
import { of } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { DialogService } from 'src/app/services/dialog.service';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let dialogService: jasmine.SpyObj<DialogService>;
  let apiService: jasmine.SpyObj<ApiService>;

  let dom = (selector: string) => fixture.nativeElement.querySelector(selector);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      providers: [
        { provide: DialogService, useFactory: () => spyOnClass(DialogService) },
        { provide: ApiService, useFactory: () => spyOnClass(ApiService) },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    dialogService = TestBed.inject(
      DialogService
    ) as jasmine.SpyObj<DialogService>;

    apiService = TestBed.inject(ApiService) as jasmine.SpyObj<ApiService>;
    const counts = require('../../../assets/counts.json');
    apiService.getUniqueCounts$.and.returnValue(of(counts));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('show display unique counts button', () => {
    expect(dom('[data-test=unique-counts-btn]')).toBeTruthy();
  });

  it('should use dialogservice when clicking on unique count button', () => {
    // grab the btn to click
    let btn = dom('[data-test=unique-counts-btn]');

    // click the btn
    btn.click();

    // assert dialogService was called
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('show display duplicated button', () => {
    expect(dom('[data-test=duplicated-btn]')).toBeTruthy();
  });

  it('should use dialogservice when clicking on duplicated button', () => {
    // grab the btn to click
    let btn = dom('[data-test=duplicated-btn]');

    // click the btn
    btn.click();

    // assert dialogService was called
    expect(dialogService.open).toHaveBeenCalled();
  });
});
