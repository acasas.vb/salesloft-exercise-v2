require("dotenv").config();
const express = require("express");
const cors = require("cors");
const http = require("http");

const { PORT = 3000 } = process.env;

const routes = require("./routes");

// API config

const app = express();

app.use(
  cors({
    origin: true,
  })
);

app.use("/", routes);

const errorHandler = (error, req, res, next) => {
  if (error) {
    res.status(error.status || 400).json({
      error: error.message,
      status: error.status || 400,
    });
  }
};

app.use(errorHandler);

const server = http.createServer(app);

server.listen(PORT, () => {
  console.log(`API Listening in port ${PORT}`);
});

module.exports = { server, errorHandler };
