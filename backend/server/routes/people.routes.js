const express = require("express");
const { getPeople, getAllPeople } = require("../utils/salesloft.util");
const { groupBySimilarity } = require("../utils/string-similarity.util");
const getUniqueCounts = require("../utils/unique-counts.util");

const peopleRouting = express.Router();

peopleRouting.get("/", (req, res, next) => {
  const { pageSize = 25, page = 1 } = req.query;
  getPeople({ pageSize, page })
    .then((data) => {
      res.json(data);
    })
    .catch((error) => {
      next(error);
    });
});

peopleRouting.get("/unique-count", async (req, res, next) => {
  try {
    let { pageSize = 100 } = req.query;
    let allPeople = await getAllPeople(pageSize);
    let counts = getUniqueCounts(
      allPeople.map((person) => person.email_address)
    );
    res.json(counts);
  } catch (error) {
    next(error);
  }
});

peopleRouting.get("/duplicated", async (req, res, next) => {
  try {
    let { precision = 0.86 } = req.query;
    let allPeople = await getAllPeople(100);
    let duplicated = groupBySimilarity(
      allPeople.map((person) => person.email_address),
      precision
    );
    res.json(duplicated);
  } catch (error) {
    console.log(error);
    next(error);
  }
});

module.exports = peopleRouting;
