const express = require("express");
const routes = express.Router();

const PeopleRouting = require("./people.routes");

routes.use("/people", PeopleRouting);

module.exports = routes;
