function getUniqueCounts(data = []) {
  let letterMap = {};
  for (let item of data) {
    item = item.toUpperCase();
    let itemParts = item.split("");
    for (let letter of itemParts) {
      if (letterMap[letter]) {
        letterMap[letter]++;
      } else {
        letterMap[letter] = 1;
      }
    }
  }

  let sortable = [];
  for (let letter in letterMap) {
    sortable.push([letter, letterMap[letter]]);
  }

  sortable
    .sort(function (a, b) {
      return a[1] - b[1];
    })
    .reverse();

  return sortable;
}

module.exports = getUniqueCounts;
