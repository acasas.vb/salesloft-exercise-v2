require("dotenv").config();
const http = require("https");

// get apiKey form env vars
const { API_KEY } = process.env;

const host = "api.salesloft.com";
const headers = {
  Authorization: `Bearer ${API_KEY}`,
};

// create a function to call people.json endpoint from salesloft
function getPeople(params = {}) {
  const { page = 1, pageSize = 25 } = params;
  return new Promise((resolve, reject) => {
    if (isNaN(page) || isNaN(pageSize)) {
      reject(new Error("page and pageSize must be numbers"));
    }
    let req = http.request(
      {
        host,
        path: `/v2/people.json?per_page=${pageSize}&page=${page}&include_paging_counts=true`,
        headers,
      },
      (response) => {
        let data = "";
        response.on("data", (chunk) => {
          data += chunk;
        });
        response.on("end", () => {
          resolve(JSON.parse(data));
        });
      }
    );
    req.on("error", function (err) {
      reject(err);
    });
    req.end();
  });
}

function getAllPeople(pageSize = 25) {
  return new Promise(async (resolve, reject) => {
    try {
      let result = [];
      let page = 1;
      do {
        let apiCallResult = await getPeople({ page, pageSize });
        result = result.concat(apiCallResult.data);
        page = apiCallResult.metadata.paging.next_page;
      } while (page);
      return resolve(result);
    } catch (error) {
      return reject(error);
    }
  });
}

module.exports = {
  getPeople,
  getAllPeople,
};
