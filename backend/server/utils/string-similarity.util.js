function levenshtein(str1 = "", str2 = "") {
  //create the levenshtein matrix
  const matrix = Array(str2.length + 1)
    .fill(null)
    .map(() => Array(str1.length + 1).fill(null));

  for (let y = 0; y <= str1.length; y += 1) {
    matrix[0][y] = y;
  }

  for (let y = 0; y <= str2.length; y += 1) {
    matrix[y][0] = y;
  }

  // calculate levenshtein distance
  for (let j = 1; j <= str2.length; j += 1) {
    for (let i = 1; i <= str1.length; i += 1) {
      const indicator = str1[i - 1] === str2[j - 1] ? 0 : 1;
      matrix[j][i] = Math.min(
        matrix[j][i - 1] + 1, // deletion
        matrix[j - 1][i] + 1, // insertion
        matrix[j - 1][i - 1] + indicator // substitution
      );
    }
  }

  // return in percentage
  return 1 / matrix[str2.length][str1.length];
}

function groupBySimilarity(data = [], precision = 0.86) {
  if (isNaN(precision)) {
    throw new Error("precision should be a number");
  }
  let _data = [...data];
  let result = [];
  for (let x = 0; x < _data.length; x++) {
    if (_data[x]) {
      let group = [_data[x]];
      for (let y = x + 1; y < _data.length; y++) {
        if (_data[y]) {
          let levenshteinDistance = levenshtein(_data[x], _data[y]);
          if (levenshteinDistance >= precision) {
            group.push(_data[y]);
            _data[y] = null;
          }
        }
      }
      if (group.length > 1) {
        result.push(group);
      }
    }
  }
  return result;
}

module.exports = {
  levenshtein,
  groupBySimilarity,
};
