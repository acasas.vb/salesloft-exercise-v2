const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
chai.use(chaiHttp);

const { PORT = 3000 } = process.env;
const { server } = require("../../server/server");

const url = `http://localhost:${PORT}`;

describe("People Router", function () {
  describe("GET /", function () {
    it("should get people records", (done) => {
      chai
        .request(server)
        .get("/people")
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should NOT get people records", (done) => {
      chai
        .request(server)
        .get("/people")
        .query({ page: "string" })
        .end((err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
  });

  describe("GET /unique-count", function () {
    it("should get unique records", (done) => {
      chai
        .request(server)
        .get("/people/unique-count")
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should NOT get unique records", (done) => {
      chai
        .request(server)
        .get("/people/unique-count?pageSize=string")
        .end((err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
  });

  describe("GET /duplicated", function () {
    it("should get duplicated records", (done) => {
      chai
        .request(server)
        .get("/people/duplicated")
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should NOT get duplicated records", (done) => {
      chai
        .request(server)
        .get("/people/duplicated?precision=string")
        .end((err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
  });
});
