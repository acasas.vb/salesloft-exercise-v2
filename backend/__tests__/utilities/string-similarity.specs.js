const chai = require("chai");
const expect = chai.expect;

const {
  levenshtein,
  groupBySimilarity,
} = require("../../server/utils/string-similarity.util");

describe("String Similarity", () => {
  describe("Levenshtein", () => {
    it("should be called", () => {
      levenshtein();
    });

    it("should return the levenshtein distance of 2 strings", () => {
      let result = levenshtein("foo", "bar");
      expect(result).to.be.equal(1 / 3);
    });

    it("should return greater than 1 levenshtein distance of same string", () => {
      let result = levenshtein("foo", "foo");
      expect(result).to.be.greaterThan(1);
    });
  });

  describe("groupBySimilarity", () => {
    it("should be called", () => {
      groupBySimilarity();
    });

    it("should group an array by similarity", () => {
      let result = groupBySimilarity([
        "foo",
        "bar",
        null,
        "benoliv@salesloft.com",
        "benolive@salesloft.com",
      ]);
      let expected = [["benoliv@salesloft.com", "benolive@salesloft.com"]];
      expect(result).to.be.deep.equal(expected);
    });

    it("should group an empty array if data is not provided", () => {
      let result = groupBySimilarity();
      let expected = [];
      expect(result).to.be.deep.equal(expected);
    });
  });
});
