const sinon = require("sinon");
const https = require("https");
const PassThrough = require("stream").PassThrough;
const chai = require("chai");
const expect = chai.expect;
const MockReq = require("mock-req");

const {
  getPeople,
  getAllPeople,
} = require("../../server/utils/salesloft.util");
const { get } = require("../../server/routes");

describe("salesloft api connection", function () {
  beforeEach(function () {
    this.request = sinon.stub(https, "request");
  });

  it("should get people from getPeople", async function () {
    let expected = require("./people.json");

    // mock the response with expected
    let response = new PassThrough();
    response.write(JSON.stringify(expected));
    response.end();

    // intercept request and use mocked data
    let request = new PassThrough();
    this.request.callsArgWith(1, response).returns(request);

    let result = await getPeople();
    expect(result).to.deep.equal(expected);
  });

  it("should pass error from getPeople", async function () {
    let expected = "page and pageSize must be numbers";

    try {
      await getPeople({ page: "string" });
    } catch (error) {
      expect(error.message).to.equal(expected);
    }
  });

  it("should catch error from request", function (done) {
    var expected = "some error";
    var request = new MockReq();

    this.request.returns(request);

    getPeople().catch(function (err) {
      expect(err).to.equal(expected);
      done();
    });

    request.emit("error", expected);
  });

  it("should get ALL people from getAllPeople", async function () {
    let total = 1;
    let expected = require("./people.json");

    // mock the response with expected
    let response = new PassThrough();
    response.write(JSON.stringify(expected));
    response.end();

    // intercept request and use mocked data
    let request = new PassThrough();
    this.request.callsArgWith(1, response).returns(request);

    let result = await getAllPeople();
    expect(result.length).to.be.equal(1);
  });

  it("should pass error from getAllPeople", async function () {
    let expected = "page and pageSize must be numbers";

    try {
      await getAllPeople({ page: "string" });
    } catch (error) {
      expect(error.message).to.equal(expected);
    }
  });

  afterEach(function () {
    https.request.restore();
  });
});
