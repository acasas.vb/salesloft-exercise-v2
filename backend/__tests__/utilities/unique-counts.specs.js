const chai = require("chai");
const expect = chai.expect;

const uniqueCounts = require("../../server/utils/unique-counts.util");

describe("unique counts", () => {
  it("should return counts of unique characters", () => {
    let data = ["ab@gmail.com", "m"];
    let expected = [
      ["M", 3],
      ["A", 2],
      ["O", 1],
      ["C", 1],
      [".", 1],
      ["L", 1],
      ["I", 1],
      ["G", 1],
      ["@", 1],
      ["B", 1],
    ];
    let result = uniqueCounts(data);
    expect(result).to.deep.equal(expected);
  });
  it("should return empty array if value is not passed", () => {
    let expected = [];
    let result = uniqueCounts();
    expect(result).to.deep.equal(expected);
  });
});
