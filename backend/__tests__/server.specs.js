require("dotenv").config();
const chai = require("chai");
const sinon = require("sinon");
const expect = chai.expect;

const { server, errorHandler } = require("../server/server");

describe("server", () => {
  it("should create an instance of express", (done) => {
    expect(server).to.not.equal(null);
    server.close(() => {
      done();
    });
  });

  it("should handle error", (done) => {
    let expected = {
      error: "Error",
      status: 400,
    };

    let fakeRes = {
      status: function () {
        return this;
      },
      json: sinon.spy(),
    };

    errorHandler(new Error("Error"), null, fakeRes);
    expect(fakeRes.json.calledOnce).to.be.true;
    expect(fakeRes.json.firstCall.args[0]).to.deep.equal(expected);
    done();
  });

  it("should not handle null error", (done) => {
    let result;
    let expected = {
      error: "Error",
      status: 400,
    };

    let fakeRes = {
      status: () => ({
        json: (data) => {
          result = data;
        },
      }),
    };

    errorHandler(null, null, fakeRes);
    expect(result).to.equal(undefined);
    done();
  });
});
