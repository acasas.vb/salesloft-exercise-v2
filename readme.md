Open in [http://salesloft.mxinventor.com](http://salesloft.mxinventor.com/)

# SalesLoft Exercise V2

## Frontend
built with angular 9, typescript

### Run tests

`npm run test`

### Run the project

`npm start`

## Backend

built with nodejs and express

### Run test

`npm run test`

### run the project

`npm start`
